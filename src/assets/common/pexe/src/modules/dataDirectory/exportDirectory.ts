import DataSection from "../binary/dataSection";
import DataBlock from "../binary/dataBlock";
import {DataType} from "../binary/dataType";
import Pexe from "../../pexe";

type ExportFunc = {
  name: string,
  ordinal: number,
}

// Описывает таблицу секций
export default class ExportDirectory extends DataSection {
  // Поля структуры
  fields = {
    //
    Characteristics: new DataBlock(DataType.DWord),
    //
    TimeDateStamp: new DataBlock(DataType.DWord),
    //
    MajorVersion: new DataBlock(DataType.Word),
    //
    MinorVersion: new DataBlock(DataType.Word),
    //
    Name: new DataBlock(DataType.DWord),
    //
    Base: new DataBlock(DataType.DWord),
    //
    NumberOfFunctions: new DataBlock(DataType.DWord),
    //
    NumberOfNames: new DataBlock(DataType.DWord),
    //
    AddressOfFunctions: new DataBlock(DataType.DWord),
    //
    AddressOfNames: new DataBlock(DataType.DWord),
    //
    AddressOfNameOrdinals: new DataBlock(DataType.DWord),
  }

  // Поля приведённые в читабельный вид
  meta = {
    // Имя библиотеки
    name: '',
    // Экспорты
    exports: [] as ExportFunc[],
  }

  private exe?: Pexe

  constructor(exe?: Pexe) {
    super();
    this.exe = exe
  }

  parse(file: DataBlock, offset: number = 0): this {
    super.parse(file, offset);
    if (!this.exe) throw new Error(`Parse can't be invoked for empty constructor`)

    // Имя библиотеки
    const nameOffset = this.exe.rvaToOffset(this.fields.Name.toNumber())
    if (nameOffset === undefined) return this
    this.meta.name = file.copyNT(nameOffset).toString()

    // Массив ординалов
    const rawPtrOrdinals = this.exe.rvaToOffset(this.fields.AddressOfNameOrdinals.toNumber())
    if (rawPtrOrdinals === undefined) throw new Error(`can't parse exportDirectory, rawPtrOrdinals is undefined`)

    const rawPtrOrdinalsPtr = file.toBlockArray(rawPtrOrdinals, DataType.Word, this.fields.NumberOfNames.toNumber())
    const ordinals = rawPtrOrdinalsPtr.map(ptrOrdinal => ptrOrdinal.toNumber())
    ordinals.forEach(ord => {
      const exp: ExportFunc = {
        name: "",
        ordinal: ord
      }
      this.meta.exports.push(exp)
    })

    // Массив имён
    const rawPtrNames = this.exe.rvaToOffset(this.fields.AddressOfNames.toNumber())
    if (rawPtrNames === undefined) throw new Error(`can't parse exportDirectory, rawPtrNames is undefined`)

    let i = 0
    for (let exp of this.meta.exports) {
      const nameOffset = file.copy(rawPtrNames + i * DataType.DWord, DataType.DWord).toNumber()
      exp.name = nameOffset !== 0 ? file.copyNT(this.exe.rvaToOffset(nameOffset)).toString() : ''
      i++
    }

    return this
  }
}
