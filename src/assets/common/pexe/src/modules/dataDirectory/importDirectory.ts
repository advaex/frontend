import DataSection from "../binary/dataSection";
import DataBlock from "../binary/dataBlock";
import {DataType} from "../binary/dataType";
import Pexe from "../../pexe";

// Описывает таблицу секций
export default class ImportDirectory extends DataSection {
  // Поля структуры
  fields = {
    //
    ImportLookupTable: new DataBlock(DataType.DWord),
    //
    TimeDateStamp: new DataBlock(DataType.DWord),
    //
    ForwarderChain: new DataBlock(DataType.DWord),
    //
    ModuleName: new DataBlock(DataType.DWord),
    //
    ImportAddressTable: new DataBlock(DataType.DWord),
  }

  // Поля приведённые в читабельный вид
  meta = {
    // Имя библиотеки
    name: '',
    // Название импортируемых методов
    funcs: [] as string[],
  }

  private exe?: Pexe

  constructor(exe?: Pexe) {
    super();
    this.exe = exe
  }

  parse(file: DataBlock, offset: number = 0) {
    super.parse(file, offset);

    // Чтение имени модуля импорта
    this.meta.name = file.copyNT(this.exe?.rvaToOffset(this.fields.ModuleName.toNumber())).toString()

    // Чтение имён импортируемых функций
    const rawLookupTable = this.exe?.rvaToOffset(this.fields.ImportLookupTable.toNumber())
    if (rawLookupTable) {
      const lookups = file.toBlockArrayNT(rawLookupTable, DataType.DWord)
      lookups.forEach(lookup => {
        // is ordinal?
        if (lookup.toNumber() & 1 << 31) {
          console.log('isOrdinal')
        } else {
          const rvaFunctionName = lookup.toNumber() & 0b011111111_11111111_11111111_11111111
          const name = file.copyNT(this.exe?.rvaToOffset(rvaFunctionName)! + DataType.Word).toString()
          this.meta.funcs.push(name)
        }
      })
    }

    return this
  }
}
