import {DataBlock, DataType, DataSection} from '../binary'

// Описывает DOS заголовок
export default class DosHeader extends DataSection {
  // Поля заголовка
  fields = {
    // Magic number (MZ)
    e_magic: new DataBlock(DataType.Word),
    // Bytes on last page of file
    e_cblp: new DataBlock(DataType.Word),
    // Pages in file
    e_cp: new DataBlock(DataType.Word),
    // Relocations
    e_crlc: new DataBlock(DataType.Word),
    // Size of header in paragraphs
    e_cparhdr: new DataBlock(DataType.Word),
    // Minimum extra paragraphs needed
    e_minalloc: new DataBlock(DataType.Word),
    // Maximum extra paragraphs needed
    e_maxalloc: new DataBlock(DataType.Word),
    // Initial (relative) SS value
    e_ss: new DataBlock(DataType.Word),
    // Initial SP value
    e_sp: new DataBlock(DataType.Word),
    // Checksum
    e_csum: new DataBlock(DataType.Word),
    // Initial IP value
    e_ip: new DataBlock(DataType.Word),
    // Initial CS value
    e_cs: new DataBlock(DataType.Word),
    // File address of relocation table
    e_lfarlc: new DataBlock(DataType.Word),
    // Overlay number
    e_ovno: new DataBlock(DataType.Word),
    // Reserved
    e_res: new DataBlock(DataType.Word * 4),
    // OEM identifier
    e_oemid: new DataBlock(DataType.Word),
    // OEM information; e_oemid specific
    e_oeminfo: new DataBlock(DataType.Word),
    // Reserved
    e_res2: new DataBlock(DataType.Word * 10),
    // File address of new exe header //TODO: Не должен выходить за пределы размера файла
    e_lfanew: new DataBlock(DataType.DWord),
  }

  // Проверят, что это валидный DOS заголовок
  isValid() {
    if (this.fields.e_magic.toString() !== 'MZ') return false
    if (this.fields.e_lfanew.toNumber() < 64) return false
    return true
  }
}
