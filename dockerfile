# docker build -f dockerfile -t advaex .
# docker rm $(docker ps -a | grep advaex | cut -c -5)
# docker run --name advaex -p 8085:80 advaex

FROM node:latest AS advaex-node
WORKDIR /root/advaex/
COPY . .
RUN npm install

FROM advaex-node AS advaex-builder
WORKDIR /root/advaex/
COPY --from=advaex-node /root/advaex/ .
RUN npm run build

FROM nginx:latest AS advaex
WORKDIR /usr/share/nginx/html
COPY --from=advaex-builder /root/advaex/dist/ .
EXPOSE 80
